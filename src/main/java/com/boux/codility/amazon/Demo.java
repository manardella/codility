/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.boux.codility.amazon;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
class Demo {

    public int solution(int[] A) {
        // write your code in Java SE 8

        int p = 1;
        int pmin = 0;
        int pmax = 0;
        int summax = 0;
        int summin = 0;

        for (int i = 0; i < A.length; i++) {
            pmin = p - 1;
            pmax = p + 1;

            summax = 0;
            for (int j = pmax; j < A.length; j++) {
                summax = summax + A[j];
            }

            summin = 0;
            for (int j = 0; j <= pmin; j++) {
                summin = summin + A[j];
            }

            if (summax == summin) {
                return p;
            }

            p++;
        }

        return -1;
    }

    public int rigthSum(int[] A, int pmax) {
        int summax = 0;
        for (int j = pmax; j < A.length; j++) {
            summax = summax + A[j];
        }

        return summax;
    }

    public int leftSum(int[] A, int pmin) {
        int summin = 0;
        for (int j = 0; j <= pmin; j++) {
            summin = summin + A[j];
        }

        return summin;
    }

}
