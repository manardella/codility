/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.boux.codility.amazon;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class AmazonTests {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int resultTestDemo = new Demo().solution(new int[]{- 1, 3, -4, 5, 1, -6, 2, 1});
        int resultTest1 = new Solution01().solution(new int[]{1, 4, -1, 3, 2});
        int resultTest2 = new Solution02().solution(new int[]{1, 2, 6, 5, 5, 8, 9});
        double resultTest3 = new Solution03().solution(new double[]{1.0, 0.1, -1.0, -7.0, 3.0, -5.0, -2.5, 0.0, 1.0});

    }

}
