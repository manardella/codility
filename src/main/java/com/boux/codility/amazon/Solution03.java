/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.boux.codility.amazon;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
class Solution03 {

    public double solution(double[] A) {
        // write your code in Java SE 8
        double maxallowed = 1000000000.0;
        double maxproduct = 0.0;
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A.length; j++) {
                double product = Pair(A, i, j);

                if (product > maxproduct) {
                    maxproduct = product;
                }
            }
        }

        if (maxproduct > maxallowed) {
            return maxallowed;
        }
        return maxproduct;
    }

    public double Pair(double[] A, int P, int Q) {
        double product = 0.0;
        for (int i = P; i <= Q; i++) {
            if (product == 0.0) {
                product = A[i];
            } else {
                product = product * A[i];
            }
        }

        return product;
    }
}
