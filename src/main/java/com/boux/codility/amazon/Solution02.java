/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.boux.codility.amazon;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
class Solution02 {

    public int solution(int[] A) {

        int result = 0;
        result = initSorting(A);

        return result;
    }

    public int initSorting(int[] A) {

        int lastvalue = 0;
        int idxstart = 0;
        int result = 0;
        boolean rangeopened = false;
        for (int i = 0; i < A.length; i++) {

            if (i == 0) {
                continue;
            }

            if (A[i] < A[i - 1]) {
                idxstart = i - 1;
                rangeopened = true;
            } else if (A[i] > A[idxstart] && rangeopened) {

                int tmpresult = i - idxstart;

                if (result == 0) {
                    result = tmpresult;
                } else if (result > tmpresult) {
                    result = tmpresult;
                }

                rangeopened = false;
            }

        }

        return result;

    }
}
