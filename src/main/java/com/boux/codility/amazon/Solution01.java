/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.boux.codility.amazon;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
class Solution01 {

    public int solution(int[] A) {

        int length = 0;
        int idx = 0;

        length = calcLength(A, length, idx);

        return length;
    }

    public int calcLength(int[] A, int length, int idx) {
        length++;

        int pointsTo = A[idx];
        int value = A[pointsTo];

        if (value != -1) {
            length++;
            length = calcLength(A, length, value);
        } else {
            length++; //last hop
        }

        return length;
    }
}
